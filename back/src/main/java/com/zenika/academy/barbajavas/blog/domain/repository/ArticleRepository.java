package com.zenika.academy.barbajavas.blog.domain.repository;

import com.zenika.academy.barbajavas.blog.domain.models.article.Article;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository extends CrudRepository<Article, String> {
    List<Article> findByUserTid(String userTid);
    @Query(value = "SELECT *\n" +
            "FROM articles\n" +
            "WHERE word_similarity(?1, articles.title) > 0.2",
            nativeQuery = true)
    Iterable<Article> findByTitle(String title);
}
