package com.zenika.academy.barbajavas.blog.web.articles;

import java.util.Optional;

public record NewArticleDto(String title, String content, String summary) {
}
