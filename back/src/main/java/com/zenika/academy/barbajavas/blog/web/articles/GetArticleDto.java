package com.zenika.academy.barbajavas.blog.web.articles;

import com.zenika.academy.barbajavas.blog.domain.models.article.ArticleState;

public record GetArticleDto(String tid, String title, String content, String summary, String author, String email, ArticleState status) {
}
