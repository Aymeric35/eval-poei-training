package com.zenika.academy.barbajavas.blog.web.users;

import com.zenika.academy.barbajavas.blog.application.UserManager;
import com.zenika.academy.barbajavas.blog.domain.models.article.Article;
import com.zenika.academy.barbajavas.blog.domain.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping
public class UserController {
    private final UserManager userManager;
    private final PasswordEncoder encoder;

    @Autowired
    public UserController(UserManager userManager, PasswordEncoder encoder) {
        this.userManager = userManager;
        this.encoder = encoder;
    }

    @PostMapping("/users/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping("/users")
    ResponseEntity<User> createNewUser(@RequestBody NewUserDto newUserDto) {
        return ResponseEntity.ok(userManager.createNewUser(newUserDto.tid(), newUserDto.username(), newUserDto.email(), encoder.encode(newUserDto.password())));
    }

    @GetMapping("/users")
    ResponseEntity<Iterable<User>> getUsers() {
        return ResponseEntity.ok(userManager.getUsers());
    }

    @GetMapping("/users/{tid}")
    ResponseEntity<Optional<User>> getUserById(@PathVariable String tid) {
        return ResponseEntity.ok(userManager.findById(tid));
    }

    @DeleteMapping("/users/{tid}")
    void deleteUserById(@PathVariable String tid) {
        userManager.deleteUser(tid);
    }

    @GetMapping("/users/{tid}/articles")
    List<Article> findUserGames(@PathVariable String tid) {
        return userManager.listArticles(tid);
    }

}
