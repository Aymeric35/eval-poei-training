package com.zenika.academy.barbajavas.blog.domain.models.article;

public enum ArticleState {
    PUBLISHED, DRAFT
}
