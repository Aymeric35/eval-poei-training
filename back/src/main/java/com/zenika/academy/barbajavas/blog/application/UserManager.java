package com.zenika.academy.barbajavas.blog.application;

import com.zenika.academy.barbajavas.blog.domain.models.article.Article;
import com.zenika.academy.barbajavas.blog.domain.models.user.User;
import com.zenika.academy.barbajavas.blog.domain.repository.ArticleRepository;
import com.zenika.academy.barbajavas.blog.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserManager {
    private final UserRepository userRepository;
    private final ArticleRepository articleRepository;

    @Autowired
    public UserManager(UserRepository userRepository, ArticleRepository articleRepository) {
        this.userRepository = userRepository;
        this.articleRepository = articleRepository;
    }

    public User createNewUser(String tid, String username, String email, String password) {
        User user = new User(UUID.randomUUID().toString(), username, email, password);
        userRepository.save(user);
        return user;
    }

    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public Optional<User> findById(String tid) {
        return userRepository.findById(tid);
    }

    public void deleteUser(String tid) {
        userRepository.deleteById(tid);
    }

    public List<Article> listArticles(String userTid) {
        if (userTid != null && !userRepository.existsById(userTid)) {
            throw new IllegalArgumentException("User does not exist.");
        }
        return articleRepository.findByUserTid(userTid);
    }
}
