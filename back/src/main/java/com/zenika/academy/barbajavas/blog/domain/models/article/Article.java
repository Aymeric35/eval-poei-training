package com.zenika.academy.barbajavas.blog.domain.models.article;

import com.zenika.academy.barbajavas.blog.domain.models.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "articles")
@Getter
@NoArgsConstructor
public class Article {
    @Id
    private String tid;
    @Setter
    private String title;
    @Setter
    private String content;
    @Setter
    private String summary;
    @ManyToOne
    @JoinColumn(name = "user_tid")
    private User user;
    private ArticleState articleState;

    public Article(String title, String content, String summary, String tid, User user) {
        this.title = title;
        this.content = content;
        this.summary = summary;
        this.tid = tid;
        this.user = user;
        this.articleState = ArticleState.DRAFT;
    }

    public void setArticleState(ArticleState articleState) {
        this.articleState = articleState;
    }
}
