package com.zenika.academy.barbajavas.blog.application;

import com.zenika.academy.barbajavas.blog.domain.models.article.Article;
import com.zenika.academy.barbajavas.blog.domain.models.article.BadLengthSummaryException;
import com.zenika.academy.barbajavas.blog.domain.models.article.NullArticleRequestException;
import com.zenika.academy.barbajavas.blog.domain.models.user.NullUserRequestException;
import com.zenika.academy.barbajavas.blog.domain.models.user.User;
import com.zenika.academy.barbajavas.blog.domain.repository.ArticleRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleManager {
    private final ArticleRepository articleRepository;
    private final UserManager userManager;

    @Autowired
    public ArticleManager(ArticleRepository articleRepository, UserManager userManager) {
        this.articleRepository = articleRepository;
        this.userManager = userManager;
    }

    public Article createNewArticle(String title, String content, String summary) throws BadLengthSummaryException, NullUserRequestException {
        if (checkNbOfWords(summary) > 50) throw new BadLengthSummaryException();
        User user = userManager.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(NullUserRequestException::new);
        Article article = new Article(title, content, summary, UUID.randomUUID().toString(), user);
        articleRepository.save(article);
        return article;
    }

    public Iterable<Article> getArticles() {
        return articleRepository.findAll();
    }

    public Iterable<Article> getAllArticleByTitle(String title){
        return articleRepository.findByTitle(title);
    }

    public Optional<Article> findById(String tid) {
        return articleRepository.findById(tid);
    }

    public Article updateArticle(String tid, String title, String summary, String content) throws NullArticleRequestException {
        Article article = articleRepository.findById(tid).orElseThrow(NullArticleRequestException::new);
        article.setTitle(title);
        article.setSummary(summary);
        article.setContent(content);
        articleRepository.save(article);
        return article;
    }

    public void deleteArticle(String tid) {
        articleRepository.deleteById(tid);
    }

    private int checkNbOfWords(String s) {
        String trim = s.trim();
        return trim.isEmpty() ? 0 : trim.split("\\s+").length;
    }
}
