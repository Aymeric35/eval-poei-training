package com.zenika.academy.barbajavas.blog.web.users;

public record NewUserDto(String tid, String username, String email, String password) {
}
