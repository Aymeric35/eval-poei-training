package com.zenika.academy.barbajavas.blog.web.articles;

import com.zenika.academy.barbajavas.blog.application.ArticleManager;
import com.zenika.academy.barbajavas.blog.domain.models.article.Article;
import com.zenika.academy.barbajavas.blog.domain.models.article.BadLengthSummaryException;
import com.zenika.academy.barbajavas.blog.domain.models.article.NullArticleRequestException;
import com.zenika.academy.barbajavas.blog.domain.models.user.NullUserRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping
public class ArticleController {
    private final ArticleManager articleManager;

    @Autowired
    public ArticleController(ArticleManager articleManager) {
        this.articleManager = articleManager;
    }

    @PostMapping("/articles")
    ResponseEntity<GetArticleDto> createNewArticle(@RequestBody NewArticleDto newArticleDto) throws BadLengthSummaryException, NullUserRequestException {
        Article article = articleManager.createNewArticle(newArticleDto.title(), newArticleDto.content(), newArticleDto.summary());
        return ResponseEntity.ok(
                new GetArticleDto(
                        article.getTid(), article.getTitle(), article.getContent(), article.getSummary(), article.getUser().getUsername(), article.getUser().getEmail(), article.getArticleState()
                )
        );
    }

    @GetMapping("/articles")
    ResponseEntity<Stream<GetArticleDto>> getArticles(@RequestParam(name="title", required = false) String title) {
        if (title != null) {
            return ResponseEntity.ok(
                    StreamSupport.stream(articleManager.getAllArticleByTitle(title).spliterator(), false).map(
                            article -> new GetArticleDto(article.getTid(), article.getTitle(), article.getContent(), article.getSummary(), article.getUser().getUsername(), article.getUser().getEmail(), article.getArticleState()
                            )
                    ));
        } else {
            return ResponseEntity.ok(
                    StreamSupport.stream(articleManager.getArticles().spliterator(), false).map(
                            article -> new GetArticleDto(article.getTid(), article.getTitle(), article.getContent(), article.getSummary(), article.getUser().getUsername(), article.getUser().getEmail(), article.getArticleState()
                            )
                    )
            );
        }
    }

    @GetMapping("/articles/{tid}")
    ResponseEntity<Optional<GetArticleDto>> getArticleById(@PathVariable String tid) {
        return ResponseEntity.ok(
                articleManager.findById(tid).map(
                        article -> new GetArticleDto(article.getTid(), article.getTitle(), article.getContent(), article.getSummary(), article.getUser().getUsername(), article.getUser().getEmail(), article.getArticleState())
                )
        );
    }

    @PutMapping("/articles/{tid}")
    ResponseEntity<Article> updateArticle(@PathVariable String tid, @RequestBody UpdateArticleDto updateArticleDto) throws NullArticleRequestException {
        return ResponseEntity.ok(articleManager.updateArticle(
                tid, updateArticleDto.title(), updateArticleDto.summary(), updateArticleDto.content()
        ));
    }

    @DeleteMapping("/articles/{tid}")
    void deleteArticle(@PathVariable String tid) {
        articleManager.deleteArticle(tid);
    }
}
