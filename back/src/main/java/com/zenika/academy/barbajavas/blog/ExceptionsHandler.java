package com.zenika.academy.barbajavas.blog;

import com.zenika.academy.barbajavas.blog.domain.models.article.BadLengthSummaryException;
import com.zenika.academy.barbajavas.blog.domain.models.article.NullArticleRequestException;
import com.zenika.academy.barbajavas.blog.domain.models.user.NullUserRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(value = BadLengthSummaryException.class)
    public final ResponseEntity<ApiError> badLengthSummaryException(BadLengthSummaryException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "The summary must not exceed 50 words."), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = NullUserRequestException.class)
    public final ResponseEntity<ApiError> nullUserRequestException(NullUserRequestException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.UNAUTHORIZED, "You must be authenticated to post a new article."), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = NullArticleRequestException.class)
    public final ResponseEntity<ApiError> nullUserRequestException(NullArticleRequestException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Article does not exist."), HttpStatus.BAD_REQUEST);
    }
}
