CREATE EXTENSION IF NOT EXISTS pg_trgm;

create table if not exists users(
    tid char(36) primary key,
    username text not null,
    email text not null unique,
    password text not null
);

create table if not exists articles(
    tid char(36) primary key,
    title text not null,
    content text not null,
    summary text not null,
    article_state text not null,
    user_tid char(36) not null references users(tid)
);
