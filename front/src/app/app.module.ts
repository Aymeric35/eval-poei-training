import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { icons, LucideAngularModule } from 'lucide-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddArticleComponent } from './pages/add-article/add-article.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ArticleComponent } from './components/article/article.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ArticleDetailsComponent } from './pages/article-details/article-details.component';
import { UpdateArticleComponent } from './pages/update-article/update-article.component';
import { RegisterComponent } from './pages/register/register.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';

@NgModule({
  declarations: [
    AppComponent,
    AddArticleComponent,
    DashboardComponent,
    ArticleComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    ArticleDetailsComponent,
    UpdateArticleComponent,
    RegisterComponent,
    MyAccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick(icons),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
