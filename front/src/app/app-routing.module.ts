import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddArticleComponent } from './pages/add-article/add-article.component';
import { ArticleDetailsComponent } from './pages/article-details/article-details.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { RegisterComponent } from './pages/register/register.component';
import { UpdateArticleComponent } from './pages/update-article/update-article.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'add-article', component: AddArticleComponent},
  { path: 'articles/:id/edit', component: UpdateArticleComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'my-account', component: MyAccountComponent},
  { path: 'articles/:id', component: ArticleDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
