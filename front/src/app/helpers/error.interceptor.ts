import { Router } from "@angular/router";
import axios from "axios";
import { AuthenticationService } from "../services/authentication.service";

export const errorInterceptor =
    (authenticationService: AuthenticationService, router: Router) => {
        axios.interceptors.response.use((response) => {
            return response;
        }, (error) => {
            if (error.response.status === 401 && router.url !== '/login') {
                sessionStorage.removeItem("currentUser");
                router.navigate(['login']);
            }
            throw error;
        })
    }