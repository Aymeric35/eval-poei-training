export interface Article {
    tid: string,
    title: string,
    summary: string,
    content: string,
    author: string
}