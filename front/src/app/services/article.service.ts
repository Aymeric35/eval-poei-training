import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../models/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  articles: Article[] = [];

  constructor() { }

  public async getArticles() {
    const response = await axios.get("http://localhost:8080/api/articles");
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
  }

  public async getArticleById(id: number) {
    const response = await axios.get("http://localhost:8080/api/articles/" + id);
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
  }

  public addArticle(article: Article) {
    return axios.post("http://localhost:8080/api/articles", {
      title: article.title,
      summary: article.summary,
      content: article.content
    })
      .then(function (response) {
        return response.data
      })
      .catch(function (error) {
        console.log(error.response.data.message);
        return error;
      });
  }

  public updateArticle(article: Article) {
    return axios.put("http://localhost:8080/api/articles/" + article.tid, {
      title: article.title,
      summary: article.summary,
      content: article.content
    })
      .then(function (response) {
        return response.data
      })
      .catch(function (error) {
        console.log(error.response.data.message);
        return error;
      });
  }

  public deleteArticle(tid: string) {
    return axios.delete(`http://localhost:8080/api/articles/${tid}`)
      .then(function (response) {
        return response
      })
      .catch(function (error) {
        console.log(error);
        return error;
      });
  }

  public async fetchArticlesByTitle(title: string) {
    return axios.get(`http://localhost:8080/api/articles`, {
      params: {
        title: title
      }
    })
      .then((res) => {
        console.log(res.data);
        return res.data;
      })
      .catch((err => {
        console.log(err);
        return err;
      }))
  }
}
