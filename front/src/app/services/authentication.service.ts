import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import axios from 'axios';
import { User } from '../models/user';
import { User as UserAuth } from '../models/userAuth';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly SESSION_STORAGE_KEY = 'currentUser';
  public isLogged: boolean = false;
  public currentLoggedUser!: UserAuth;

  constructor(private router: Router) { }

  async register(user: User) {
    try {
      const response = await axios.post("http://localhost:8080/api/users", {
        username: user.username,
        email: user.email,
        password: user.password
      });
      return response.data;
    } catch (error) {
      return error;
    }
  }

  async credentialsValidation(user: UserAuth) {
    return axios.get("http://localhost:8080/api/users/me", {
      auth: {
        username: user.username,
        password: user.password
      }
    })
  }

  async login(user: UserAuth) {
    await this.credentialsValidation(user).then(response => {
      sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
      sessionStorage.setItem('isLogged', 'true');
      this.isLogged = true;
      this.currentLoggedUser = user;
      this.router.navigate(['']);
    }).catch(error => {
      console.log('incorrect credentials', error.response.status)
    });
  }

  logout() {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
    sessionStorage.setItem('isLogged', 'false');
    this.isLogged = false;
    this.router.navigate(['']);
  }

  getCurrentUserBasicAuthentication(): string {
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain);
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password)
    } else {
      return "";
    }
  }
}
