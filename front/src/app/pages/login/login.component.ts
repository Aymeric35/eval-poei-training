import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string = "";
  password: string = "";

  constructor(public authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.username = "";
    this.password = "";
  }

  public login(): void {
    if (!this.username || !this.password) return;
    this.authenticationService.login({username: this.username, password: this.password});
  }

}
