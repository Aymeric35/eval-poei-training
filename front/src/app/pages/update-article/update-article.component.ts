import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.scss']
})
export class UpdateArticleComponent implements OnInit {
  article: Article = {
    tid: "",
    title: "",
    author: "",
    summary: "",
    content: ""
  }

  author: string = "";

  constructor(public articleService: ArticleService, private router: Router, private activeRoute: ActivatedRoute) { }

  async getArticle() {
    this.activeRoute.params.subscribe(async params => {
      this.article = await this.articleService.getArticleById(params['id']);
    })
  }


  async updateArticle() {
    await this.articleService.updateArticle(this.article);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {
    this.getArticle();
  }

}
