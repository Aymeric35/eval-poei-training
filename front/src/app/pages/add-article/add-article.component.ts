import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss']
})
export class AddArticleComponent implements OnInit {
  article: Article = {
    tid: "",
    title: "",
    author: JSON.parse(sessionStorage.getItem('currentUser') || '').username,
    summary: "",
    content: ""
  }

  constructor(public articleService: ArticleService, private router: Router) { }

  async addArticle() {
    await this.articleService.addArticle(this.article);
    this.router.navigate(['/']);
  }

  ngOnInit(): void {

  }

}
