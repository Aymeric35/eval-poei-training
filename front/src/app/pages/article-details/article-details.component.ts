import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.scss']
})
export class ArticleDetailsComponent implements OnInit {
  public article?: Article

  constructor(private articleService: ArticleService, private activeRoute: ActivatedRoute, private router: Router, public authenticationService: AuthenticationService) { }

  async getArticle() {
    this.activeRoute.params.subscribe(async params => {
      this.article = await this.articleService.getArticleById(params['id']);
    })
  }

  async deleteArticle(tid: string) {
    this.articleService.deleteArticle(tid)
    .then(() => this.router.navigate(['']));
  }

  ngOnInit(): void {
    this.getArticle();
  }

}
